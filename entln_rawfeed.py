import enipy as lx
import time

# create the feed object (note <partner ID> needs to be filled in)
feed = lx.FeedReceiver( partnerId="<partner ID>", feedType=1, decode=False )
feed.start()
# enter main loop
while True:
	if len(feed.received ) == 0:
		# no lightning yet
		time.sleep(.1)
		continue
		
	# print all the messages
	while len(feed.received) > 0:
		S= feed.received.pop(0) 
		print S
