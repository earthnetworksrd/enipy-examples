import enipy as lx	#so we can read the data
import enipy.visTools as vt	#so we can make the map object
import matplotlib.pyplot as plt	#for normal plotting stuff

MAP_BBOX   = [ [-40, -10], [-80, -30] ] #[[lats],[lons]]
MAP_ZOOM   = 5	#larger numbers is more detail
# the file we want to load, this path points to the sample data directory 
# (which is not always included in the distribution)
inFile = '../sample_data/20170201-20170201-lxversion.V3-lxtype.Flash-stroketype.Both-0.csv.gz'

data = lx.Report( inFile ) # load the data

figMap, figAx = vt.make_map( MAP_BBOX, zoom=MAP_ZOOM ) # create the map

figMap.drawcoastlines( color='b') # optional

# plot the data in the normal way for basemap, not the latlon=True option
figMap.plot( data.lon, data.lat, 'r.', ms=2, alpha=0.2, latlon=True )

plt.show() # draw the image on the screen

