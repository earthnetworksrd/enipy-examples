import enipy as lx
import enipy.visTools as vt
import time
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np


#~ MAP_BBOX = [ [-00, 15 ],[-20,0 ] ]
#~ MAP_ZOOM = 6
MAP_BBOX = [ [-50, 50 ],[-180,179 ] ]
MAP_ZOOM = 4


# create the feed object (note <partner ID> needs to be filled in)
# for this one, I've requested a pulse feed, instead of a flash feed
feed = lx.FeedReceiver( partnerId="<partner ID>", feedType=2 )
feed.start()


figMap, figAx = vt.make_map( MAP_BBOX, zoom=MAP_ZOOM ) # create the map
figAx.figure.subplotpars = mpl.figure.SubplotParams(left=0,right=1,bottom=0,top=1 )
figAx.figure.show()	#draw the map


# create a report object to put the pulses from the feed into
report = lx.Report()

# update the density map every minute
tDensity = time.time()
pulseDenColl = None	#initialize this to something
pulsePCG = None
pulseNCG = None

# enter main loop
while True:
	if len(feed.received ) == 0:
		# no lightning yet
		plt.pause(0.00001)
		figAx.figure.canvas.blit( figAx.bbox )
		continue
	
	# print all the messages
	while len(feed.received) > 0:
		pulse = feed.received.pop(0) 
		
		#append that to an open Report Object
		report.append( pulse )

	#clear the plot of dots, but not density or images
	if pulsePCG != None:
		pulsePCG.remove()
		del pulsePCG
	if pulseNCG != None:
		pulseNCG.remove()
		del pulseNCG

		
	tNow = time.time()
	
	print tNow, report.time.max()
	
	# mask for +CGs in the last 10 minutes
	m = (report.type==0)&(report.amplitude>0)&(tNow - report.time < 60)
	x, y = figMap( report.lon[m], report.lat[m] )
	print len( x ), '+CGs'
	pulsePCG,  = figMap.plot( x,  y,  'o', mfc=(1,0,0,.7), mec='None', ms=6  )

	# mask for +CGs in the last 10 minutes
	m = (report.type==0)&(report.amplitude<0)&(tNow - report.time < 60)
	x, y = figMap( report.lon[m], report.lat[m] )
	print len( x ), '-CGs'
	pulseNCG,  = figMap.plot( x,  y,  'o', mfc=(.1,.3,1,.7), mec='None', ms=3  )




	#update the density?
	if tNow-tDensity > 15:	#in seconds
		print 'updating density'
		# we'll also truncate the data as well
		m = (tNow - report.time < 900)&\
			(report.lat>MAP_BBOX[0][0])&(report.lat<MAP_BBOX[0][1])&\
			(report.lon>MAP_BBOX[1][0])&(report.lon<MAP_BBOX[1][1])
		# this keeps only values where m=True
		report.truncate( m )
		

		###
		# plot the bulk data as a density map
		# first, remove the old data, if there is any
		if pulseDenColl != None:
			for coll in pulseDenColl.collections:
				coll.remove()
			del pulseDenColl		

		# second, calculate the new density
		pulseDen = vt.pulse_density( report.lat, report.lon, MAP_BBOX, pixelSize=0.1 )

		# then draw
		# cmTransGray is a transparent color map which looks like clouds
		# cmTransJet  is a transparent color map which a bit like radar		
		x,y = np.meshgrid( pulseDen[1][:-1], pulseDen[2][:-1] )
		pulseDenColl = figMap.contourf( x, y, np.log10( pulseDen[0].T+1 ), 5, cmap=vt.cmTransJet, vmin=.1, vmax=2, latlon=True )
		# we can do the same thing with pcolor and get pixel maps, but the drawing is much slower
		#pulseDenColl = figMap.pcolormesh( x, y, np.log10( pulseDen[0].T+1 ), cmap=vt.cmTransGray, vmin=0, vmax=2, latlat=True )

		tDensity = time.time()

	print '%i pulses in report'%len(report.lat)


	###
	# we updated the plot, so we need to update things
	#do I need both of these?
	#~ figAx.figure.canvas.blit( figAx.bbox )
	plt.pause(0.000001)
