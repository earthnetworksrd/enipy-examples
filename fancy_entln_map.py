import enipy  as lx	#so we can read the data
import enipy.visTools as vt	#so we can make the map object
import matplotlib.pyplot as plt	#for normal plotting stuff
import numpy as np

MAP_BBOX   = [ [-40, -10], [-80, -30] ] #[[lats],[lons]]
MAP_ZOOM   = 5	#larger numbers is more detail
# the file we want to load
inFile = '../sample_data/20170201-20170202-lxversion.V3-lxtype.Portion-stroketype.Both-0.csv.gz'

data = lx.Report( inFile ) # load the data

###
# create the map
# I want fine detailed coast lines, so I'm passing the 'resolution' 
# argument to Basemap
figMap, figAx = vt.make_map( MAP_BBOX, zoom=MAP_ZOOM, resolution='l' )

###
# draw the coastlines, this is optional but shows the accuracy of the 
# map images
figMap.drawcoastlines( color='k')

###
# lets get the lat-lon lines
vt.draw_map_grid( figMap )

###
# plot the bulk data as a density map
# make a mask of the data for all sources older than the last 5 minutes 
# in the report
m = data.time < data.time.max()-300
# calculate the pulse density
# the mask I've made is backwards
pulseDen = vt.pulse_density( data.lat[m], data.lon[m], MAP_BBOX, pixelSize=0.2 )

# convert to map coordinates, 
# this is more complicated because lat and lon may not be the same shape
#~ x, tmp = figMap( pulseDen[1], pulseDen[1]  )
#~ tmp, y = figMap( pulseDen[2], pulseDen[2]  )
#~ # display the density (the slowish way)
#~ # cmTransGray is a transparent color map which will make the lightning 
#~ # locations look kindof like clouds
#~ figMap.pcolor( x, y, np.log10( pulseDen[0].T+1 ), cmap=vt.cmTransGray )

x,y = np.meshgrid( pulseDen[1][:-1], pulseDen[2][:-1] )
coll = figMap.contourf( x, y, np.log10( pulseDen[0].T+1 ), 5, cmap=vt.cmTransJet, edgecolor=(0,0,0,0), vmin=0, vmax=4, latlon=True )


###
# plot CG strokes colored by polarity
# make a mask of the negative CGs newer than 5 minutes, we'll plot these 
# in blue
m = (data.time > data.time.max()-300)&(data.type == 0)&(data.amplitude<0)
# first, convert lat and lon to x and y
x, y = figMap( data.lon[m], data.lat[m] )
# then, plot the data using the map's methods
figMap.plot( x, y, 'b.', ms=4, alpha=0.5 )

###
# plot the data in the usual mpl.Basemap way
# make a mask of the positive CGs newer than 5 minutes, we'll plot these 
# in red
m = (data.time > data.time.max()-300)&(data.type == 0)&(data.amplitude>0)
# first, convert lat and lon to x and y
x, y = figMap( data.lon[m], data.lat[m] )
# then, plot the data using the map's methods.  We'll make the positive 
# CG's bigger since there are probably not so many
figMap.plot( x, y, 'r.', ms=10, alpha=0.5 )

###
# finish up by giving the map a title
figAx.set_title( 'Lightning in South America on Feb 1, 2017' )
figAx.set_xlabel( 'Longitude' )
figAx.set_ylabel( 'Latitude' )

###
# draw the image on the screen
plt.show()
