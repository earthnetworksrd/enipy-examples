#!/usr/bin/python 
########################################################################
# Modification of Enipy
# Tools for extracting flash and pulse data from ENTLN server 
#   
# Copyright (c) 2017 Earth Networks, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without res\]triction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

#Python library declarion
import socket
import time
import os
import shutil
import threading
import gzip

""" Partner ID declaration for flash and the pulse feed """
partnerId = {"flash" : "D752DB5B-D818-4DA0-A371-FAF73A4E1326",
            "pulse": "B7E52FEF-B991-4455-A959-862BB9FD61F8"}

""" Feed parser class which can be used to create multiple objects 
to invoke the socket connection to the ENTLN server"""

class FeedParser():
    def __init__(self, id=None, format=1, type=1, decodeIp=True):
        self.partnerId = id
        self.version = 2
        self.feedFormat = format
        self.feedType = type

        self.ip = ["184.72.125.75", "107.23.153.83"]
        self.port = 2324
        self.decode = decodeIp

        self.ipband = 0

    def openSocket(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((self.ip[self.ipband], self.port))
        requeststr = '{"p":"%s","v":%i,"f":%i,"t":%i,"class":3,"meta":true}' % (
        self.partnerId, self.version, self.feedFormat, self.feedType)
        self.socket.sendall(requeststr.encode("utf-8"))
        print("Socket connected!!")

    def closeSocket(self):
        self.socket.close()

    def get_packet(self):
        try:
            return self.socket.recv(1024)
        except:
            print('socket failed')
            self.closeSocket()
            self.ipband = (self.ipband + 1) % len(self.ip)
            self.openSocket()

""" Compress the old files that are oilder than 24 hours old """            
    def compress(self,filename):
        for root, dirs, files in os.walk(filename):
            for file in files:
                if file != (str(time.strftime("%d-%m-%Y")) + ".txt") and file.endswith(".txt") \
                and os.path.getctime(root + file) < twodays_ago:
                    with open(root + file, 'rb') as f_in:
                        with gzip.open(root + file + ".gz", 'wb') as f_out:
                            shutil.copyfileobj(f_in, f_out)
                    os.remove(root + file)
                    
                    
""" Puill data from the socket for one hour """            
    def run(self,filename):
        now = time.time() 
        twodays_ago = now - 1
        self.compress(filename)
        if not os.path.exists(filename):
            os.makedirs(filename)
        self.openSocket()
        t_end = time.time() + 60 * 60 + 10
        while time.time() < t_end:
            try:
                with open(filename + str(time.strftime("%d-%m-%Y")) + ".txt", "a+") as f:
                    f.write(self.get_packet().decode('utf-8'))
            except:
                flash() if self.feedType == 1 else pulse()

""" Objects declaration to access the  feed parser class"""            

def flash():
    feed1 = FeedParser(id=partnerId['flash'], format=1, type=1)
    while True:
        feed1.run(os.path.expanduser('~') + "/feed/flash/")

        
def pulse():
    feed2 = FeedParser(id=partnerId['pulse'], format=1, type=2)
    while True:
        feed2.run(os.path.expanduser('~') + "/feed/pulse/")
        
""" Multithreading to simentaousely download the flash and the pulse feed in a seperate files"""
def Main():
    t1 = threading.Thread(target=flash)
    t2 = threading.Thread(target=pulse)
    t1.start()
    t2.start()
    t1.join()
    t2.join()

Main()
