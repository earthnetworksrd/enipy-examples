import enipy as lx
import time

# create the feed object (note <partner ID> needs to be filled in)
feed = lx.FeedReceiver( partnerId="<partner ID>", feedType=2 )
feed.start()
# enter main loop
while True:
	if len(feed.received ) == 0:
		# no lightning yet
		time.sleep(.1)
		continue
		
	# print all the messages
	while len(feed.received) > 0:
		f= feed.received.pop(0) 
		S = f.printout()
		print S
